import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import pages.HomePage;
import pages.ResultSearchPage;

import static io.github.bonigarcia.wdm.WebDriverManager.firefoxdriver;

public class BaseTest {
    private WebDriver driver;
    private static final String AVIC_URL = "https://avic.ua/";

    @BeforeTest
    public void preSetup(){
        firefoxdriver().setup();
    }

    @BeforeMethod
    public void testSetup(){
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get(AVIC_URL);
    }

    @AfterMethod
    public void postSetup() {
        driver.quit();
    }

    public WebDriver getDriver(){
        return driver;
    }

    public HomePage getHomePage(){
        return new HomePage(getDriver());
    }

    public ResultSearchPage getResultSearchPage(){
        return new ResultSearchPage(getDriver());
    }

}
