import org.testng.Assert;
import org.testng.annotations.Test;

public class ResultSearchTest extends BaseTest {
    private static final String SEARCH_KEYWORD = "Xiaomi Mi 11";
    private static final String EXPECTED_QUERY = "query=Xiaomi+Mi+11";
    private static final String SEARCH = "Смартфон Xiaomi 9";


    @Test
    public void checkRightSearchWord() {
        getHomePage().checkRightSearch(SEARCH_KEYWORD);
        Assert.assertTrue(getDriver().getCurrentUrl().contains(EXPECTED_QUERY));
    }




}
