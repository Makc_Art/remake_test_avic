import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ImageDetailsOnPageHome extends BaseTest{
    private static final int EXPECTED_AMOUNT_ITEMS = 13;
    private static final String[] LIST_ITEMS = new String[]{"Apple Store", "Смартфоны и телефоны", "Компьютеры", "Ноутбуки и планшеты", "Телевизоры и аксессуары",
            "Бытовая техника", "Аудиотехника", "Гаджеты", "Аксессуары", "Game Zone", "Фото-видео техника", "Умный дом", "Электротранспорт"};
    private static final String DELIVERY = "Оплата і доставка";
    private static final String SECTION = "Корпоративний відділ";

    @Test
    public void checkAmountListItems(){
        getResultSearchPage().getListItems();
        getHomePage().implicitWait(20);
        Assert.assertEquals(getResultSearchPage().getListItemsCount(),EXPECTED_AMOUNT_ITEMS);
    }

    @Test
    public void checkListItems(){
        int i = 0;
        getResultSearchPage().getListItems();
        getHomePage().implicitWait(20);
        for (WebElement element : getResultSearchPage().getListItems() ) {
            Assert.assertEquals(element.getText(), LIST_ITEMS[i]);
            i++;
        }
    }

    @Test
    public void checkChangeLanguage(){
        getHomePage().pageUa();
        Assert.assertEquals(getHomePage().delivery.getText(),DELIVERY);
        Assert.assertEquals(getHomePage().section.getText(),SECTION);
    }
}
