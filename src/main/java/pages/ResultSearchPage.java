package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;


public class ResultSearchPage extends Base {

    @FindBy(xpath = "//span[@class='sidebar-item']")
    private WebElement sidebarItem;

    @FindBy(xpath = "//ul[@class='sidebar-list sidebar-list--fl']/li[@class='parent js_sidebar-item']//span[@class='sidebar-item-title']/span")
    private List<WebElement> listItems;

    public ResultSearchPage(WebDriver driver) {
        super(driver);
    }

    public List<WebElement> getListItems(){
        sidebarItem.click();
        return listItems;
    }

    public int getListItemsCount(){
        return getListItems().size();
    }

}
