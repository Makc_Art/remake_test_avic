package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends Base {

    @FindBy(xpath = "//input[@class='search-query']")
    private WebElement searchInput;

    @FindBy(xpath = "//button[contains(@class, 'search-btn') and @type='submit']")
    private WebElement searchButton;

    @FindBy(xpath = "//li[@class='has-dropdown']/ul[contains(@class,'menu-dropdown__lang')]//a")
    private WebElement urlUa;

    @FindBy(xpath = "//ul[contains(@class,'two-column')]//a[contains(@href,'dostavka-tovarov')]")
    public WebElement delivery;

    @FindBy(xpath = "//ul[contains(@class,'two-column')]//a[contains(@href,'optovyij-otdel')]")
    public  WebElement section;

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public void checkRightSearch(String keyword) {
        searchInput.sendKeys(keyword);
        searchButton.click();
    }

    public void pageUa(){
        driver.get(urlUa.getAttribute("href"));
    }
}
